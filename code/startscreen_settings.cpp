//
// Created by bepho on 6/10/19.
//
#include "startscreen_settings.hpp"
#include <iostream>

startscreen_settings::startscreen_settings(std::shared_ptr<sf::RenderWindow> window) : startscreen(window)
{
  if(window.get())
  {
    process();
  }
}

startscreen_settings::~startscreen_settings()
{

}

bool startscreen_settings::handle_events()
{
  sf::Event event;
  while (window_->pollEvent(event))
  {
    // Close window: exit
    if (event.type == sf::Event::Closed)
    {
      window_->close();
      return false;
    }

    if (event.type == sf::Event::KeyPressed)
    {
      switch (event.key.code)
      {
        case sf::Keyboard::S:
        case sf::Keyboard::Down:
        {
          std::cout << "pressed down" << std::endl;
          selection_index_++;
          if(selection_index_ >= menu_entries_.size())
          {
            selection_index_ = 0;
          }
          break;
        }

        case sf::Keyboard::W:
        case sf::Keyboard::Up:
        {
          std::cout << "pressed up" << std::endl;
          selection_index_--;
          if(selection_index_ >= menu_entries_.size())
          {
            selection_index_ = menu_entries_.size() - 1;
          }
          break;
        }

        case sf::Keyboard::A:
        case sf::Keyboard::Left:
        {
          std::cout << "pressed left" << std::endl;
          break;
        }

        case sf::Keyboard::D:
        case sf::Keyboard::Right:
        {
          std::cout << "pressed right" << std::endl;
          break;
        }

        case sf::Keyboard::Enter:
        {
          std::cout << "pressed enter" << std::endl;
          if(selection_index_ == menu_entries_.size() - 1)
          {
            return false;
          }
          break;
        }
      }

      // update menu style
      for(auto iter = menu_entries_.begin(); iter != menu_entries_.end(); ++iter)
      {
        iter->setStyle(sf::Text::Style::Regular);
      }
      menu_entries_.at(selection_index_).setStyle(sf::Text::Style::Underlined);
    }
  }

  return true;
}

int startscreen_settings::process()
{
  if(!menu_font_.loadFromFile("/home/bepho/Dev/gitlab/jump_and_run_maker/res/fonts/FiraCode-Bold.ttf"))
  {
    return EXIT_FAILURE;
  }

  // adding entries to menu
  sf::Text text_new_game("window resolution", menu_font_, 30);
  text_new_game.setPosition(0,0);
  text_new_game.setStyle(sf::Text::Style::Underlined);
  menu_entries_.push_back(text_new_game);

  sf::Text text_continue("go back", menu_font_, 30);
  text_continue.setPosition(0,40);
  menu_entries_.push_back(text_continue);

  while (window_->isOpen())
  {
    // Process events
    if(!handle_events())
    {
      window_->clear();
      return EXIT_SUCCESS;
    }

    // Clear screen
    window_->clear();


    // Draw menu
    for(auto iter = menu_entries_.begin(); iter != menu_entries_.end(); ++iter)
    {
      window_->draw(*iter);
    }

    // Update the window
    window_->display();
  }

  return EXIT_SUCCESS;
}
