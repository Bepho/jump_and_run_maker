//
// Created by bepho on 6/10/19.
//
#include "startscreen.hpp"
#include "startscreen_settings.hpp"
#include "game.hpp"

#include <iostream>

startscreen::startscreen() : window_(new sf::RenderWindow(sf::VideoMode(1600, 1000), "jump and run")), menu_font_(), initialized_(true), selection_index_(0)
{
  window_->setVerticalSyncEnabled(true);
  //window_->setFramerateLimit(60);
}

startscreen::startscreen(std::shared_ptr<sf::RenderWindow> window) : window_(window), menu_font_(), initialized_(true), selection_index_(0)
{
}

startscreen::~startscreen()
{

}

int startscreen::run()
{
  return process();
}

int startscreen::process()
{
  if(!menu_font_.loadFromFile("/home/bepho/Dev/gitlab/jump_and_run_maker/res/fonts/FiraCode-Bold.ttf"))
  {
    return EXIT_FAILURE;
  }

  // adding entries to menu
  sf::Text text_new_game("new game", menu_font_, 30);
  text_new_game.setPosition(0,0);
  text_new_game.setStyle(sf::Text::Style::Underlined);
  menu_entries_.push_back(text_new_game);

  sf::Text text_continue("continue", menu_font_, 30);
  text_continue.setPosition(0,40);
  menu_entries_.push_back(text_continue);

  sf::Text text_settings("settings", menu_font_, 30);
  text_settings.setPosition(0, 80);
  menu_entries_.push_back(text_settings);

  sf::Text text_quit("quit", menu_font_, 30);
  text_quit.setPosition(0, 120);
  menu_entries_.push_back(text_quit);

  while (window_->isOpen())
  {
    // Process events
    if(!handle_events())
    {
      window_->clear();
      return EXIT_SUCCESS;
    }

    // Clear screen
    window_->clear();

    // Draw menu
    for(auto iter = menu_entries_.begin(); iter != menu_entries_.end(); ++iter)
    {
      window_->draw(*iter);
    }

    // Update the window
    window_->display();
  }

  return EXIT_SUCCESS;
}

bool startscreen::handle_events()
{
  sf::Event event;
  while (window_->pollEvent(event))
  {
    // Close window: exit
    if (event.type == sf::Event::Closed)
    {
      window_->close();
      return false;
    }

    if (event.type == sf::Event::KeyPressed)
    {
      switch (event.key.code)
      {
        case sf::Keyboard::S:
        case sf::Keyboard::Down:
        {
          std::cout << "pressed down" << std::endl;
          selection_index_++;
          if(selection_index_ >= menu_entries_.size())
          {
            selection_index_ = 0;
          }
          break;
        }

        case sf::Keyboard::W:
        case sf::Keyboard::Up:
        {
          std::cout << "pressed up" << std::endl;
          selection_index_--;
          if(selection_index_ >= menu_entries_.size())
          {
            selection_index_ = menu_entries_.size() - 1;
          }
          break;
        }

        case sf::Keyboard::Enter:
        {
          std::cout << "pressed enter" << std::endl;
          switch (selection_index_)
          {
            case menu_entry_new_game_:
            {
              game gm(window_);
              break;
            }
            case menu_entry_continue_:
            {
              break;
            }
            case menu_entry_settings_:
            {
              startscreen_settings settings(window_);
              break;
            }
            case menu_entry_quit_:
            {
              window_->close();
              return false;
            }
            default:
            {
              std::cout << "unknown menu entry: " << selection_index_ << std::endl;
              break;
            }
          }

          break;
        }
      }

      // update menu style
      for(auto iter = menu_entries_.begin(); iter != menu_entries_.end(); ++iter)
      {
        iter->setStyle(sf::Text::Style::Regular);
      }
      menu_entries_.at(selection_index_).setStyle(sf::Text::Style::Underlined);
    }
  }

  return true;
}

