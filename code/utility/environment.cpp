//
// Created by bepho on 6/13/19.
//

#include "environment.hpp"
#include <iostream>


Environment::Environment() : scale_(5, 5), tile_size_(8,8)
{
  // loading tileset map
  tile_set_.loadFromFile("/home/bepho/Dev/gitlab/jump_and_run_maker/res/sprites/tileset/cavesofgallet_tiles.png");

  {
    sprite_set_ = utility::parse_map_file("/home/bepho/Dev/gitlab/jump_and_run_maker/res/map_data/map_01.json", scale_, tile_set_);
  }
  // setting up test map
  /*
  for (int row = 0; row < 14; ++row)
  {
    utility::sprite_row__ sprite_row;
    for (int column = 0; column < 20; ++column)
    {
      sf::Sprite spr(tile_set_, sf::IntRect(0, 0, tile_size_.x, tile_size_.y));
      spr.setPosition(tile_size_.x * column * scale_.x, tile_size_.y * row * scale_.y);
      spr.scale(scale_);
      sprite_row.push_back(std::make_pair(spr,utility::property_bitset__().reset()));
    }
    sprite_set_.push_back(sprite_row);
  }
  {
    utility::sprite_row__ sprite_row;
    for (int column = 0; column < 20; ++column)
    {
      sf::Sprite spr(tile_set_, sf::IntRect(8 * 4, 8 * 4, tile_size_.x, tile_size_.y));
      spr.setPosition(tile_size_.x * column * scale_.x, tile_size_.y * 14 * scale_.y);
      spr.scale(scale_);
      sprite_row.push_back(std::make_pair(spr,utility::property::property_solid_bit_));
    }
    sprite_set_.push_back(sprite_row);
  }
   */
}

utility::property_bitset__ Environment::get_tile_property(sf::Vector2f location)
{
  sf::Vector2i row_column_info = get_row_column(location);

  std::cout << "tileset reference x: " << row_column_info.x << " y: " << row_column_info.y << std::endl;

  return sprite_set_[row_column_info.y][row_column_info.x].second;
}

sf::Vector2f Environment::get_tile_position(sf::Vector2f location)
{
  sf::Vector2i row_column_info = get_row_column(location);
  return sprite_set_[row_column_info.y][row_column_info.x].first.getPosition();
}

sf::IntRect Environment::get_tile_size(sf::Vector2f location)
{
  sf::Vector2i row_column_info = get_row_column(location);
  return sprite_set_[row_column_info.y][row_column_info.x].first.getTextureRect();
}

sf::Vector2i Environment::get_row_column(sf::Vector2f location)
{
  sf::Vector2i row_column_info;
  if(location.x < 1 && location.x > -1 && location.y < 1 && location.y > -1)
  {
    row_column_info.x = 0;
    row_column_info.y = 0;
  }
  else if(location.x < 1 && location.x > -1)
  {
    row_column_info.x = 0;
    row_column_info.y = static_cast<int>(location.y) / static_cast<int>(scale_.y) / tile_size_.y;
  }
  else if(location.y < 1 && location.y > -1)
  {
    row_column_info.x = static_cast<int>(location.x) / static_cast<int>(scale_.x) / tile_size_.x;
    row_column_info.y = 0;
  }
  else
  {
    row_column_info.x = static_cast<int>(location.x) / static_cast<int>(scale_.x) / tile_size_.x;
    row_column_info.y = static_cast<int>(location.y) / static_cast<int>(scale_.y) / tile_size_.y;
  }
  return row_column_info;
}

utility::sprite_set__& Environment::get_sprite_set()
{
  return sprite_set_;
}