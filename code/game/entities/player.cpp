//
// Created by bepho on 6/12/19.
//

#include "player.hpp"
#include "global_config.hpp"
#include <iostream>

Player::Player() : animation_bits_(0), state_bits_(0), action_bits_(0),
                   process_movement_thread_(&process_movement, this), process_animation_thread_(&process_state, this), x_velocity_(0), y_velocity_(0)
{
  // load complete frame sheet
  if (!player_texture_.loadFromFile(
    "/home/bepho/Dev/gitlab/jump_and_run_maker/res/sprites/player/adventurer-v1.5-Sheet.png"))
  {
    std::cout << "could not load player texture" << std::endl;
  }
  setTexture(player_texture_);

  // set animation frames relative to the texture
  // idle
  idle_normal_.push_back(sf::IntRect(50 * 0, 37 * 0, 50, 37));
  idle_normal_.push_back(sf::IntRect(50 * 1, 37 * 0, 50, 37));
  idle_normal_.push_back(sf::IntRect(50 * 2, 37 * 0, 50, 37));
  idle_normal_.push_back(sf::IntRect(50 * 3, 37 * 0, 50, 37));

  // idle attack
  idle_attack_.push_back(sf::IntRect(50 * 3, 37 * 5, 50, 37));
  idle_attack_.push_back(sf::IntRect(50 * 4, 37 * 5, 50, 37));
  idle_attack_.push_back(sf::IntRect(50 * 5, 37 * 5, 50, 37));
  idle_attack_.push_back(sf::IntRect(50 * 6, 37 * 5, 50, 37));

  // attack 1
  attack_1_.push_back(sf::IntRect(50 * 0, 37 * 6, 50, 37));
  attack_1_.push_back(sf::IntRect(50 * 1, 37 * 6, 50, 37));
  attack_1_.push_back(sf::IntRect(50 * 2, 37 * 6, 50, 37));
  attack_1_.push_back(sf::IntRect(50 * 3, 37 * 6, 50, 37));
  attack_1_.push_back(sf::IntRect(50 * 4, 37 * 6, 50, 37));

  // attack 2
  attack_2_.push_back(sf::IntRect(50 * 5, 37 * 6, 50, 37));
  attack_2_.push_back(sf::IntRect(50 * 6, 37 * 6, 50, 37));
  attack_2_.push_back(sf::IntRect(50 * 0, 37 * 7, 50, 37));
  attack_2_.push_back(sf::IntRect(50 * 1, 37 * 7, 50, 37));
  attack_2_.push_back(sf::IntRect(50 * 2, 37 * 7, 50, 37));
  attack_2_.push_back(sf::IntRect(50 * 3, 37 * 7, 50, 37));

  // attack 3
  attack_3_.push_back(sf::IntRect(50 * 4, 37 * 7, 50, 37));
  attack_3_.push_back(sf::IntRect(50 * 5, 37 * 7, 50, 37));
  attack_3_.push_back(sf::IntRect(50 * 6, 37 * 7, 50, 37));
  attack_3_.push_back(sf::IntRect(50 * 0, 37 * 8, 50, 37));
  attack_3_.push_back(sf::IntRect(50 * 1, 37 * 8, 50, 37));
  attack_3_.push_back(sf::IntRect(50 * 2, 37 * 8, 50, 37));

  // jump
  jump_.push_back(sf::IntRect(50 * 0, 37 * 2, 50, 37));
  jump_.push_back(sf::IntRect(50 * 1, 37 * 2, 50, 37));
  jump_.push_back(sf::IntRect(50 * 2, 37 * 2, 50, 37));
  jump_.push_back(sf::IntRect(50 * 3, 37 * 2, 50, 37));

  // fall
  fall_.push_back(sf::IntRect(50 * 1, 37 * 3, 50, 37));
  fall_.push_back(sf::IntRect(50 * 2, 37 * 3, 50, 37));

  // die
  die_.push_back(sf::IntRect(50 * 2, 37 * 3, 50, 37));
  die_.push_back(sf::IntRect(50 * 2, 37 * 3, 50, 37));
  die_.push_back(sf::IntRect(50 * 2, 37 * 3, 50, 37));
  die_.push_back(sf::IntRect(50 * 2, 37 * 3, 50, 37));
  die_.push_back(sf::IntRect(50 * 2, 37 * 3, 50, 37));
  die_.push_back(sf::IntRect(50 * 2, 37 * 3, 50, 37));
  die_.push_back(sf::IntRect(50 * 2, 37 * 3, 50, 37));

  // run
  run_.push_back(sf::IntRect(50 * 1, 37 * 1, 50, 37));
  run_.push_back(sf::IntRect(50 * 2, 37 * 1, 50, 37));
  run_.push_back(sf::IntRect(50 * 3, 37 * 1, 50, 37));
  run_.push_back(sf::IntRect(50 * 4, 37 * 1, 50, 37));
  run_.push_back(sf::IntRect(50 * 5, 37 * 1, 50, 37));
  run_.push_back(sf::IntRect(50 * 6, 37 * 1, 50, 37));

  setTextureRect(idle_normal_[1]);

  state_bits_.set(utility::state::state_ground_);
}

Player::~Player()
{
  terminate_.store(true, std::memory_order_relaxed);
  process_movement_thread_.join();
  process_animation_thread_.join();
}

void Player::move(utility::action::type direc, bool enable)
{
  std::lock_guard lock(bit_sync_);
  action_bits_.set(direc, enable);
}

void Player::process_movement(void *param)
{
  std::cout << "started player movement thread" << std::endl;
  auto parent = static_cast<Player *>(param);

  if (parent)
  {
    sf::Clock time_step;
    time_step.restart();
    std::this_thread::sleep_for(std::chrono::milliseconds(50));
    while (!parent->terminate_.load(std::memory_order_relaxed))
    {
      // calculate velocity

      if (parent->action_bits_[utility::action::action_left_])
      {
        parent->x_velocity_ -=
          global_config_nmsp::player::run_acceleration * (time_step.getElapsedTime().asSeconds());

        if(parent->x_velocity_ < -global_config_nmsp::player::max_velocity)
        {
          parent->x_velocity_ = -global_config_nmsp::player::max_velocity;
        }
      }
      else if(parent->x_velocity_ < 0.0)
      {
        parent->x_velocity_ +=
          global_config_nmsp::player::run_acceleration * (time_step.getElapsedTime().asSeconds());

        if(parent->x_velocity_ > 0)
        {
          parent->x_velocity_ = 0.0;
        }
      }

      if (parent->action_bits_[utility::action::action_right_])
      {
        parent->x_velocity_ +=
          global_config_nmsp::player::run_acceleration * time_step.getElapsedTime().asSeconds();

        if(parent->x_velocity_ > global_config_nmsp::player::max_velocity)
        {
          parent->x_velocity_ = global_config_nmsp::player::max_velocity;
        }
      }
      else if(parent->x_velocity_ > 0.0)
      {
        parent->x_velocity_ -=
          global_config_nmsp::player::run_acceleration * (time_step.getElapsedTime().asSeconds());

        if(parent->x_velocity_ < 0)
        {
          parent->x_velocity_ = 0.0;
        }
      }

      if(parent->action_bits_[utility::action::action_jump_])
      {
        if(parent->state_bits_[utility::state::state_ground_])
        {
          parent->y_velocity_ = -global_config_nmsp::player::max_velocity;
        }
      }

      parent->y_velocity_ +=
        global_config_nmsp::player::run_acceleration * (time_step.getElapsedTime().asSeconds());

      if(parent->y_velocity_ > global_config_nmsp::player::max_velocity)
      {
        parent->y_velocity_ = global_config_nmsp::player::max_velocity;
      }

      if (parent->y_velocity_ < -global_config_nmsp::player::max_velocity)
      {
        parent->y_velocity_ = -global_config_nmsp::player::max_velocity;
      }

      sf::Vector2f predicted_displacement(parent->getPosition().x + parent->x_velocity_,
                                          parent->getPosition().y + parent->y_velocity_);

      // check colisions
      parent->check_collision(predicted_displacement);

      // apply velocity
      {
        std::lock_guard lock(parent->map_sync_);
        parent->setPosition(predicted_displacement);
      }

      //std::cout << "curent state: " << (parent->state_bits_.any() ? "on ground" : "in air") << std::endl;
      //std::cout << "player pos x: " << predicted_displacement.x << " y: " << predicted_displacement.y << std::endl;

      time_step.restart();
      std::this_thread::sleep_for(std::chrono::milliseconds(1000 / 60));

    }
  }
  std::cout << "terminated player movement thread" << std::endl;
}

void Player::process_state(void *param)
{
  std::cout << "started player animation thread" << std::endl;
  auto parent = static_cast<Player *>(param);

  if (parent)
  {
    sf::Clock time_step;
    while (!parent->terminate_.load(std::memory_order_relaxed))
    {
      /*
      time_step.restart();
      while (parent->state_bits_[utility::state::state_ground_] && !parent->terminate_.load(std::memory_order_relaxed))
      {
        if(parent->x_velocity_ == 0)
        {
          utility::animate(parent->idle_normal_, 0.1, param);
        }
        time_step.restart();
        while(parent->x_velocity_ != 0 && !parent->terminate_)
        {
          std::lock_guard lock(parent->sync_);
          parent->setTextureRect(parent->run_[(static_cast<int>(time_step.getElapsedTime().asSeconds() * 5)) % parent->run_.size()]);
        }
        std::this_thread::sleep_for(std::chrono::milliseconds(1000/60));
      }

      for(auto iter = parent->idle_normal_.begin(); iter != parent->idle_normal_.end(); ++iter)
      {
        //parent->draw_sync_->lock();
        //parent->setTextureRect(parent->idle_normal_[0]);
        //parent->sync_.lock();
        {
          //std::lock_guard lock(parent->sync_);
          parent->setTextureRect(*iter);
        }
        std::this_thread::sleep_for(std::chrono::milliseconds(1000/60));
        //parent->sync_.unlock();
      }

      std::this_thread::sleep_for(std::chrono::milliseconds(1000/60));
      std::lock_guard lock(parent->sync_);
      //parent->setTextureRect(parent->idle_normal_[parent->idle_normal_.size() % static_cast<int>(time_step.getElapsedTime().asSeconds())]);
      */
      std::this_thread::sleep_for(std::chrono::milliseconds(1000/60));
    }
  }
  std::cout << "terminated player animation thread" << std::endl;
}

void Player::set_map(Environment &map)
{
  std::lock_guard lock(map_sync_);
  map_ = &map;
}

void Player::check_collision(sf::Vector2f &displacement)
{
  if(map_)
  {
    sf::Vector2f predicted_displacement = getPosition();
    predicted_displacement.x += x_velocity_;
    predicted_displacement.y += y_velocity_;

    state_bits_.reset();

    if (x_velocity_ > 0)
    {


    } else
    {

    }

    if (y_velocity_ > 0)
    {
      if (map_->get_tile_property(
        sf::Vector2f(getPosition().x, predicted_displacement.y + getTextureRect().height + 4)).test(utility::property::property_solid_))
      {
        auto tile_pos = map_->get_tile_position(predicted_displacement);
        displacement.y = tile_pos.y;
        y_velocity_ = 0;
        state_bits_.set(utility::state::state_ground_);
      }
    }
    else
    {

    }
  }
}