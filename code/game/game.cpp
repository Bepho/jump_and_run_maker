//
// Created by bepho on 6/11/19.
//
#include "game.hpp"
#include "player.hpp"
#include <iostream>

game::game(std::shared_ptr<sf::RenderWindow> window) : window_(window)
{
  player_.set_map(map1_);

  if (window.get())
  {
    process();
  }
}

int game::process()
{
  auto map = map1_.get_sprite_set();

  while (window_->isOpen())
  {
    // Process events
    if (!handle_events())
    {
      window_->clear();
      return EXIT_SUCCESS;
    }

    // Clear screen
    window_->clear();

    // Draw
    // background


    for (auto iter = map.begin(); iter != map.end(); ++iter)
    {
      for(auto iter2 = iter->begin(); iter2 != iter->end(); ++iter2)
      {
        window_->draw(iter2->first);
      }
    }

    window_->draw(player_.draw_synchronized());

    // Update the window
    window_->display();
  }

  return EXIT_SUCCESS;
}

bool game::handle_events()
{
  sf::Event event;
  while (window_->pollEvent(event))
  {
    // Close window: exit
    if (event.type == sf::Event::Closed)
    {
      window_->close();
      return false;
    }

    if (event.type == sf::Event::KeyPressed)
    {
      switch (event.key.code)
      {
        case sf::Keyboard::S:
        case sf::Keyboard::Down:
        {
          std::cout << "pressed down" << std::endl;
          //player_sprite_.setPosition(player_sprite_.getPosition().x, player_sprite_.getPosition().y + 5);
          player_.move(utility::action::action_down_, true);
          break;
        }

        case sf::Keyboard::W:
        case sf::Keyboard::Up:
        {
          std::cout << "pressed up" << std::endl;
          //player_sprite_.setPosition(player_sprite_.getPosition().x, player_sprite_.getPosition().y - 5);
          player_.move(utility::action::action_up_, true);
          break;
        }

        case sf::Keyboard::A:
        case sf::Keyboard::Left:
        {
          std::cout << "pressed left" << std::endl;
          //player_sprite_.setPosition(player_sprite_.getPosition().x - 5, player_sprite_.getPosition().y);
          player_.move(utility::action::action_left_, true);
          break;
        }

        case sf::Keyboard::D:
        case sf::Keyboard::Right:
        {
          std::cout << "pressed right" << std::endl;
          //player_sprite_.setPosition(player_sprite_.getPosition().x + 5, player_sprite_.getPosition().y);
          player_.move(utility::action::action_right_, true);
          break;
        }

        case sf::Keyboard::Space:
        {
          std::cout << "pressed space" << std::endl;
          //player_sprite_.setPosition(player_sprite_.getPosition().x + 5, player_sprite_.getPosition().y);
          player_.move(utility::action::action_jump_, true);
          break;
        }

        case sf::Keyboard::Enter:
        {
          std::cout << "pressed enter" << std::endl;
          break;
        }

        case sf::Keyboard::Escape:
        {
          std::cout << "pressed escape" << std::endl;
          return false;
        }
      }
    } else if (event.type == sf::Event::KeyReleased)
    {
      switch (event.key.code)
      {
        case sf::Keyboard::S:
        case sf::Keyboard::Down:
        {
          std::cout << "released down" << std::endl;
          player_.move(utility::action::action_down_, false);
          break;
        }

        case sf::Keyboard::W:
        case sf::Keyboard::Up:
        {
          std::cout << "released up" << std::endl;
          player_.move(utility::action::action_up_, false);
          break;
        }

        case sf::Keyboard::A:
        case sf::Keyboard::Left:
        {
          std::cout << "released left" << std::endl;
          player_.move(utility::action::action_left_, false);
          break;
        }

        case sf::Keyboard::D:
        case sf::Keyboard::Right:
        {
          std::cout << "released right" << std::endl;
          player_.move(utility::action::action_right_, false);
          break;
        }

        case sf::Keyboard::Space:
        {
          std::cout << "pressed released" << std::endl;
          //player_sprite_.setPosition(player_sprite_.getPosition().x + 5, player_sprite_.getPosition().y);
          player_.move(utility::action::action_jump_, false);
          break;
        }

        case sf::Keyboard::Enter:
        {
          std::cout << "released enter" << std::endl;
          break;
        }

        case sf::Keyboard::Escape:
        {
          std::cout << "released escape" << std::endl;
          return false;
        }
      }
    }
  }
  return true;
}