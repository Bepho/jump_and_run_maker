//
// Created by bepho on 6/10/19.
//

#ifndef SFML_TEST_STARTSCREEN_HPP
#define SFML_TEST_STARTSCREEN_HPP

#include <SFML/Graphics.hpp>
#include <vector>
#include <memory>

class startscreen
{
public:
  startscreen();
  startscreen(std::shared_ptr<sf::RenderWindow> window);
  ~startscreen();

  int run();

private:
  int process();
  bool handle_events();

  bool initialized_;

  enum menu_entries : size_t
  {
    menu_entry_new_game_,
    menu_entry_continue_,
    menu_entry_settings_,
    menu_entry_quit_
  };

protected:
  std::shared_ptr<sf::RenderWindow> window_;
  std::vector<sf::Text> menu_entries_;
  size_t selection_index_;
  sf::Font menu_font_;
};


#endif //SFML_TEST_STARTSCREEN_HPP
