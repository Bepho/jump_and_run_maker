//
// Created by bepho on 6/13/19.
//

#ifndef JUMP_AND_RUN_MAKER_SPRITE_LOCKABLE_HPP
#define JUMP_AND_RUN_MAKER_SPRITE_LOCKABLE_HPP

#include <SFML/Graphics.hpp>
#include <atomic>
#include <mutex>

class SpriteLockable : public sf::Sprite
{
public:
  SpriteLockable() : terminate_(false) {}
  sf::Sprite draw_synchronized()
  {
    std::lock_guard lock(sync_);
    return *this;
  }

  std::mutex sync_;
  std::atomic_bool terminate_;
};
#endif //JUMP_AND_RUN_MAKER_SPRITE_LOCKABLE_HPP
