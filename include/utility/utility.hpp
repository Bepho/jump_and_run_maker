//
// Created by bepho on 6/13/19.
//

#ifndef JUMP_AND_RUN_MAKER_UTILITY_HPP
#define JUMP_AND_RUN_MAKER_UTILITY_HPP

#include <nlohmann/json.hpp>
#include "sprite_lockable.hpp"
#include "../../extern/nlohmann/json.hpp"
#include <vector>
#include <bitset>
#include <fstream>
#include <iostream>
#include <memory>
#include <global_config.hpp>

namespace utility
{
  namespace state
  {
    enum type
    {
      state_ground_, state_air_, state_wall_, state_water_, states_
    };
  };

  namespace action
  {
    enum type
    {
      action_left_, action_right_, action_up_, action_down_, action_jump_, actions_
    };
  };

  namespace property
  {
    enum type
    {
      property_solid_, properties_,
    };

    enum bit
    {
      property_solid_bit_ = 0x1, property_bits_
    };
  }

  struct tile_info
  {
    std::uint16_t number_;
    std::uint16_t orientation_;

    tile_info() : number_(0), orientation_(0)
    {}
  };

  namespace tile_orientation
  {
    enum type
    {
      vertical_flipped_ = 0x8000, horizontal_flipped_ = 0x4000, diagonal_ = 0x2000
    };
  }

  namespace sprite
  {
    enum type : std::uint16_t
    {
      stone_ground_ = 12,
      stone_wall_ = 19,
      grass_ground_ = 9,
      grass_cliff = 10,
      wood_cliff_ = 22
    };
  }

  typedef std::bitset<property::properties_> property_bitset__;
  typedef std::pair<sf::Sprite, property_bitset__> sprite_property__;
  typedef std::vector<sprite_property__> sprite_row__;
  typedef std::vector<sprite_row__> sprite_set__;

  typedef std::vector<sf::IntRect> animation_sequence__;

  static void animate(animation_sequence__ &sequence, float frame_time_seconds, void *param)
  {
    auto userdata = static_cast<SpriteLockable *>(param);
    if (userdata)
    {
      //while (userdata->terminate_.load())
    }
  }

  static sprite_set__ parse_map_file(std::string map_file_location, sf::Vector2f scale, sf::Texture &texture)
  {
    sprite_set__ ss;
    std::ifstream input_file(map_file_location.c_str());
    if (input_file.is_open())
    {
      nlohmann::json root;
      root = nlohmann::json::parse(input_file);
      int map_width = root["width"];
      int map_height = root["height"];


      // get tilesheet information
      nlohmann::json tilesets = root["tilesets"].begin().value();
      auto tile_count = tilesets["tilecount"].get<std::uint32_t>();
      auto tile_width = tilesets["tilewidth"].get<std::uint32_t>();
      auto tile_height = tilesets["tileheight"].get<std::uint32_t>();
      auto image_width = tilesets["imagewidth"].get<std::uint32_t>();
      auto image_heigth = tilesets["imageheight"].get<std::uint32_t>();
      auto tile_columns = tilesets["columns"].get<std::uint32_t>();

      // get tile coordinates
      auto map_coords = root["layers"].begin().value()["data"].get<std::vector<std::uint32_t>>();

      // fill map
      auto map_coords_iter = map_coords.begin();
      for (size_t rows = 0; rows < root["height"].get<size_t>(); ++rows)
      {
        sprite_row__ sprite_row;
        for (size_t columns = 0; columns < root["width"].get<size_t>(); ++columns)
        {
          if (map_coords_iter != map_coords.end())
          {
            sprite_property__ sp;
            tile_info ti;
            {
              auto tmp = *map_coords_iter;
              std::memcpy(&ti, &tmp, sizeof(tmp));
            }
            if (ti.number_ != 0)
            {
              // set texture
              sp.first.setTexture(texture);

              // cut sprite from sheet
              sf::Vector2i texture_pos;
              sf::Vector2i texture_size(tile_width, tile_height);
              texture_pos.x = ((ti.number_ - 1) % tile_columns) * tile_width;
              if(ti.number_ - 1 == 0)
              {
                texture_pos.y = 0;
              }
              else
              {
                texture_pos.y = ((ti.number_ -1) / tile_columns) * tile_height;
              }

              // place sprite in map
              sp.first.setTextureRect(sf::IntRect(texture_pos, texture_size));
              sp.first.setPosition(tile_width * columns * scale.x, tile_height * rows * scale.y);
              sp.first.scale(scale);

              // apply properties
              switch (ti.number_ - 1)
              {
                case utility::sprite::stone_ground_:
                case utility::sprite::stone_wall_:
                case utility::sprite::grass_ground_:
                case utility::sprite::grass_cliff:
                case utility::sprite::wood_cliff_:
                {
                  sp.second.set(utility::property::property_solid_, true);
                  break;
                }
                default:
                {
                  sp.second.reset();

                  break;
                }
              }
              sprite_row.push_back(sp);
              std::cout << "number: " << ti.number_ << " orientation: " << ti.orientation_ << std::endl;
            }
            ++map_coords_iter;
          }
        }
        ss.push_back(sprite_row);
      }
    }
    return ss;
  }


}
#endif //JUMP_AND_RUN_MAKER_UTILITY_HPP
