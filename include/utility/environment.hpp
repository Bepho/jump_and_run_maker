//
// Created by bepho on 6/13/19.
//

#ifndef JUMP_AND_RUN_MAKER_ENVIRONMENT_HPP
#define JUMP_AND_RUN_MAKER_ENVIRONMENT_HPP

#include "utility.hpp"

class Environment
{
public:
  Environment();

  utility::property_bitset__ get_tile_property(sf::Vector2f location);
  sf::Vector2f get_tile_position(sf::Vector2f location);
  sf::IntRect get_tile_size(sf::Vector2f location);

  utility::sprite_set__& get_sprite_set();
private:
  sf::Vector2i get_row_column(sf::Vector2f location);

  sf::Texture           tile_set_;
  utility::sprite_set__ sprite_set_;
  sf::Vector2i          tile_size_;
  sf::Vector2f          scale_;

};
#endif //JUMP_AND_RUN_MAKER_ENVIRONMENT_HPP
