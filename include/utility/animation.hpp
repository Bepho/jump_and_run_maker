//
// Created by bepho on 6/12/19.
//

#ifndef JUMP_AND_RUN_MAKER_ANIMATION_HPP
#define JUMP_AND_RUN_MAKER_ANIMATION_HPP

#include <SFML/Graphics.hpp>
#include <thread>
#include <mutex>


class Animation : public sf::IntRect
{
public:
  Animation(std::string path_to_texture);

  void play();
private:

  std::thread t_;
};
#endif //JUMP_AND_RUN_MAKER_ANIMATION_HPP
