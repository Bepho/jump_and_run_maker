//
// Created by bepho on 6/10/19.
//

#ifndef SFML_TEST_STARTSCREEN_SETTINGS_HPP
#define SFML_TEST_STARTSCREEN_SETTINGS_HPP

#include "startscreen.hpp"


class startscreen_settings : protected startscreen
{
public:
  startscreen_settings(std::shared_ptr<sf::RenderWindow> window);
  ~startscreen_settings();

private:
  int process();
  bool handle_events();
};
#endif //SFML_TEST_STARTSCREEN_SETTINGS_HPP
