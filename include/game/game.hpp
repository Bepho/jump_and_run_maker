//
// Created by bepho on 6/10/19.
//

#ifndef SFML_TEST_GAME_HPP
#define SFML_TEST_GAME_HPP

#include "player.hpp"
#include "environment.hpp"

#include <SFML/Graphics.hpp>
#include <memory>
#include <mutex>

class game
{
public:
  game(std::shared_ptr<sf::RenderWindow> window);

private:
  int process();
  bool handle_events();

  std::shared_ptr<sf::RenderWindow> window_;
  Player player_;
  Environment map1_;
};
#endif //SFML_TEST_GAME_HPP
