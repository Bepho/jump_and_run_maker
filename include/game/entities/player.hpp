//
// Created by bepho on 6/12/19.
//

#ifndef JUMP_AND_RUN_MAKER_PLAYER_HPP
#define JUMP_AND_RUN_MAKER_PLAYER_HPP

#include <SFML/Graphics.hpp>
#include <vector>
#include <bitset>
#include <thread>
#include <mutex>
#include <condition_variable>
#include "utility.hpp"
#include "environment.hpp"

class Player : public SpriteLockable
{
public:

  struct animation_type
  {
    enum type
    {
      animation_type_idle_normal_,
      animation_type_idle_attack_,
      animation_type_attack_1_,
      animation_type_attack_2_,
      animation_type_attack_3_,
      animation_type_jump_,
      animation_type_fall_,
      animation_type_die_,
      animation_type_run_,
      animation_types_
    };
  };


  Player();

  ~Player();

  void set_map(Environment &map);

  void move(utility::action::type direc, bool enable);

private:
  /// \brief checks action bits (move()) and applies velocity on the player. before applying the new position,
  ///        the function checks for collisions (check_collision()) and applies depending on the tile, a new state to
  ///        the player
  /// \param param pointer to its own class
  static void process_movement(void *param);

  /// \brief checks current state of player and absolute direction of movement to determine the correct annimation
  ///        sequence
  /// \param param pointer to its own class
  static void process_state(void *param);

  void check_collision(sf::Vector2f &displacement);

  sf::Texture player_texture_;
  utility::animation_sequence__ idle_normal_;
  utility::animation_sequence__ idle_attack_;
  utility::animation_sequence__ attack_1_;
  utility::animation_sequence__ attack_2_;
  utility::animation_sequence__ attack_3_;
  utility::animation_sequence__ jump_;
  utility::animation_sequence__ fall_;
  utility::animation_sequence__ die_;
  utility::animation_sequence__ run_;

  std::mutex bit_sync_;
  std::bitset<animation_type::animation_types_> animation_bits_;
  std::bitset<utility::state::states_> state_bits_;
  std::bitset<utility::action::actions_> action_bits_;
  std::condition_variable input_event_;

  std::thread process_movement_thread_;
  std::thread process_animation_thread_;

  float x_velocity_;
  float y_velocity_;
  Environment *map_;
  std::mutex map_sync_;
};

#endif //JUMP_AND_RUN_MAKER_PLAYER_HPP
