//
// Created by bepho on 6/13/19.
//

#ifndef JUMP_AND_RUN_MAKER_GLOBAL_CONFIG_HPP
#define JUMP_AND_RUN_MAKER_GLOBAL_CONFIG_HPP
namespace global_config_nmsp
{
  const float gravity = 4.0;

  namespace player
  {
    const float max_velocity = 5.0;
    const float run_acceleration = 8.0;
    const float jump_inital_acceleration = 4.0;
  }
}
#endif //JUMP_AND_RUN_MAKER_GLOBAL_CONFIG_HPP
